package com.login.login.repo;

import com.login.login.dao.LoginDao;
import org.springframework.data.repository.CrudRepository;

public interface LoginRepo extends CrudRepository<LoginDao, String> {
}
