package com.login.login.dao;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class LoginDao {
    @Id
    private String userEmail;
    private String userName;
    private String userPassword;

    public LoginDao() {

    }

    public LoginDao(String userEmail, String userName, String userPassword) {
        this.userEmail = userEmail;
        this.userName = userName;
        this.userPassword = userPassword;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }
}
