package com.login.login.controller;

import com.login.login.dao.LoginDao;
import com.login.login.repo.LoginRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LoginController {
    @Autowired
    LoginRepo repo;
    @CrossOrigin(origins = "http://localhost:3000")
    @PostMapping(path="/register", consumes={"application/json"})
    public String registerUser(@RequestBody LoginDao registration) {
    repo.save(registration);
    return "success";
    };
}
