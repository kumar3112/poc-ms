package com.product.product.service;

import com.product.product.exceptions.FileStorageException;
import com.product.product.exceptions.MyFileNotFoundException;
import com.product.product.model.DBFile;
import com.product.product.model.GetModel;
import com.product.product.model.ModelList;
import com.product.product.model.ProductDetails;
import com.product.product.repository.DBFileRepository;
import com.product.product.repository.ProductDetailsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

import java.sql.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class DBFileStorageService {

    @Autowired
    private DBFileRepository dbFileRepository;
    @Autowired
    private ProductDetailsRepository productDetailsRepository;


    public DBFile storeFile(MultipartFile file) {
        // Normalize file name
        String fileName = StringUtils.cleanPath(file.getOriginalFilename());

        try {
            // Check if the file's name contains invalid characters
            if(fileName.contains("..")) {
                throw new FileStorageException("Sorry! Filename contains invalid path sequence " + fileName);
            }

            DBFile dbFile = new DBFile(fileName, file.getContentType(), file.getBytes());

            return dbFileRepository.save(dbFile);
        } catch (IOException ex) {
            throw new FileStorageException("Could not store file " + fileName + ". Please try again!", ex);
        }
    }

    public DBFile getFile(String fileId) {
        return dbFileRepository.findById(fileId)
                .orElseThrow(() -> new MyFileNotFoundException("File not found with id " + fileId));
    }

    public List<DBFile> getAll() {
        return (List<DBFile>) dbFileRepository.findAll();
    }

    public List<ArrayList> getProducts() {
        List<ArrayList> products = dbFileRepository.getProducts();

        return products;
    }

    public void saveProductDetails(ProductDetails productDetails) {
        productDetailsRepository.save(productDetails);

    }

    public List<ProductDetails> getAllProduct() {
        return (List<ProductDetails>) productDetailsRepository.findAll();
    }

}
