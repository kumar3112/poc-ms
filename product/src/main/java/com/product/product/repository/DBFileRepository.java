package com.product.product.repository;

import com.product.product.model.DBFile;
import com.product.product.model.GetModel;
import com.product.product.model.ModelList;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public interface DBFileRepository extends JpaRepository<DBFile, String> {

    @Query("select f.id, f.fileName, f.fileType from DBFile f")
    List<ArrayList> getProducts();
}
