package com.product.product.controller;

import com.product.product.model.DBFile;
import com.product.product.model.GetModel;
import com.product.product.model.ModelList;
import com.product.product.model.ProductDetails;
import com.product.product.payload.UploadFileResponse;
import com.product.product.repository.ProductDetailsRepository;
import com.product.product.service.DBFileStorageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;



import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@RestController
public class FileController {

    private static final Logger logger = LoggerFactory.getLogger(FileController.class);

    @Autowired
    private DBFileStorageService dbFileStorageService;
    @CrossOrigin(origins = "http://localhost:3000")
    @PostMapping("/uploadFile")
    public UploadFileResponse uploadFile(@RequestParam("file") MultipartFile file) {
        DBFile dbFile = dbFileStorageService.storeFile(file);

        String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
                .path("/downloadFile/")
                .path(dbFile.getId())
                .toUriString();

        return new UploadFileResponse(dbFile.getFileName(), fileDownloadUri,
                file.getContentType(), file.getSize(), dbFile.getId());
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @PostMapping(path="/productDetails", consumes={"application/json"})
    public void uploadProductDetails(@RequestBody ProductDetails productDetails) {
        dbFileStorageService.saveProductDetails(productDetails);
    }

    @PostMapping("/uploadMultipleFiles")
    public List<UploadFileResponse> uploadMultipleFiles(@RequestParam("files") MultipartFile[] files) {
        return Arrays.asList(files)
                .stream()
                .map(file -> uploadFile(file))
                .collect(Collectors.toList());
    }


//    @CrossOrigin(origins = "http://localhost:3000")
//    @GetMapping("/products")
//    public List<GetModel> downloadFile() {
//        // Load file from database
//        List<DBFile>  products =  dbFileStorageService.getAll();
//
//        List<GetModel> list = new ArrayList<>();
//        products.forEach(
//                item ->     {
//                    GetModel model = new GetModel();
//                    model.setFileName(item.getFileName());
//                    model.setFileType(item.getFileType());
//                    model.setId(item.getId());
//                    list.add(model);
//                });
//
//        return list;
//
//    }

    @CrossOrigin(origins = "http://localhost:3000")
    @GetMapping("/products")
    public List<ProductDetails> downloadFile() {
        // Load file from database
        List<ProductDetails>  products =  dbFileStorageService.getAllProduct();
         return products;

    }








//    @CrossOrigin(origins = "http://localhost:3000")
//    @GetMapping("/products")
//    public List<GetModel> downloadFile() {
//        // Load file from database
//        List<ArrayList>  products =  dbFileStorageService.getProducts();
//
//        List<GetModel> list = new ArrayList<>();
//        products.forEach(
//            item ->     {
//                GetModel model = new GetModel();
//                model.setFileName(item.get(1));
//                model.setFileType(item.get(2));
//                model.setId(item.get(0));
//                list.add(model);
//            });
//
//        return list;
//
//    }

//    @CrossOrigin(origins = "http://localhost:3000")
//    @GetMapping("/products")
//    public ResponseEntity<Resource> downloadFile() {
//        // Load file from database
//        List<DBFile>  dbFile =  dbFileStorageService.getAll();
//
//        return ResponseEntity.ok()
//                .contentType(MediaType.parseMediaType(dbFile.getFileType()))
//                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + dbFile.getFileName() + "\"")
//                .body(new ByteArrayResource(dbFile.getData()));
//    }

    @GetMapping("/downloadFile/{fileId}")
    public ResponseEntity<Resource> downloadFile(@PathVariable String fileId) {
        // Load file from database
        DBFile dbFile = dbFileStorageService.getFile(fileId);

        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(dbFile.getFileType()))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + dbFile.getFileName() + "\"")
                .body(new ByteArrayResource(dbFile.getData()));
    }


}
