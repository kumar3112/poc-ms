package com.category.category.dao;

import com.category.category.entity.Category;
import org.springframework.data.repository.CrudRepository;

public interface CategoryDao extends CrudRepository<Category, String> {

}
