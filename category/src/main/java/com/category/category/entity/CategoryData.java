package com.category.category.entity;

import java.util.List;

public class CategoryData {
    List<Category> category;

    public List<Category> getCategory() {
        return category;
    }

    public void setCategory(List<Category> category) {
        this.category = category;
    }
}
