package com.category.category.entity;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Category {
    @Id
    private String CategoryId;
    private String CategoryName;
    private int DisplayOrder;
    private boolean IsActive;

    public Category(String categoryId, String categoryName, int displayOrder, boolean isActive) {
        CategoryId = categoryId;
        CategoryName = categoryName;
        DisplayOrder = displayOrder;
        IsActive = isActive;
    }

    public Category() {

    }

    public String getCategoryId() {
        return CategoryId;
    }

    public void setCategoryId(String categoryId) {
        CategoryId = categoryId;
    }

    public String getCategoryName() {
        return CategoryName;
    }

    public void setCategoryName(String categoryName) {
        CategoryName = categoryName;
    }

    public int getDisplayOrder() {
        return DisplayOrder;
    }

    public void setDisplayOrder(int displayOrder) {
        DisplayOrder = displayOrder;
    }

    public boolean isActive() {
        return IsActive;
    }

    public void setActive(boolean active) {
        IsActive = active;
    }
}
