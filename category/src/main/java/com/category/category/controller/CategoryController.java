package com.category.category.controller;

import com.category.category.dao.CategoryDao;
import com.category.category.entity.Category;
import com.category.category.entity.CategoryData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class CategoryController {
    @Autowired
    CategoryDao dao;

    @PostMapping(path="/category", consumes={"application/json"})
    public void setCategory(@RequestBody Category category) {
    dao.save(category);
    }

    @GetMapping(path="/category")
    public CategoryData setCategory() {
        List<Category> cat = (List<Category>) dao.findAll();
        CategoryData data = new CategoryData();
        data.setCategory(cat);
        return data;
    }
}
