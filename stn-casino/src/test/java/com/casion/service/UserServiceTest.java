package com.casion.service;

import com.casion.MockUtility;
import com.casion.entity.User;
import com.casion.entity.UserData;
import com.casion.framework.StnCasinoApplication;
import com.casion.repository.UserRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.Assert;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes={UserService.class})
class UserServiceTest {

    @MockBean
    private UserRepository userRepository;

    @Autowired
    private UserService userService;

    private List<User> users;

    @BeforeEach
    void setUp() {
        ClassLoader loader = getClass().getClassLoader();
        users = MockUtility.getUserData(loader.getResource("Users.json").getFile());
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void getAllUsers() {
        Mockito.when(userRepository.findAll()).thenReturn(users);
        Assertions.assertEquals(1,userService.getAllUsers().getUsers().get(0).getUserId());
    }

//    @Test
//    void addUser() {
//    }
}