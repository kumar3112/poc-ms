package com.casion;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.casion.entity.User;
import com.casion.entity.UserData;
import org.apache.commons.io.IOUtils;
import org.json.JSONObject;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;


public class MockUtility {
	
	private static ObjectMapper objMapper = null;

	static {
		objMapper = new ObjectMapper();
		objMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
	}
	
	public static <T> T deserialize(Class<T> clazz, InputStream is) {
		try {
			return objMapper.readValue(is, clazz);
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}

	public static List<User> getUserData(String filePath) {
//		UserData ud = new UserData();
		List<User> users = new ArrayList<>();
		User user = new User();
		File file = new File(filePath);
		try {
			FileInputStream fileIS = new FileInputStream(file);
			String jsonString = IOUtils.toString(fileIS, "UTF-8");
			JSONObject jsonObject = new JSONObject(jsonString);
			user.setUserId(jsonObject.optLong("userId"));
			user.setUserName(jsonObject.optString("userName"));
			//ud.setUsers(Arrays.asList(user));
			users.add(user);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return users;
	}

/*

	public static FindCentivoSelectPractitionersRequest getFindCentivoSelectPractitionersRequest() {
		FindCentivoSelectPractitionersRequest request = new FindCentivoSelectPractitionersRequest();
		request.setContextMemberUuid("96d0d6af-a785-3c8b-b7f3-85943e025b64");
		request.setPatientUuid("96d0d6af-a785-3c8b-b7f3-85943e025b64");
		request.setMemberUuid("96d0d6af-a785-3c8b-b7f3-85943e025b64");
		request.setPcpFlag(true);
		
		CentivoSelectProviderFilter centivoSelectProviderFilter = new CentivoSelectProviderFilter();
		centivoSelectProviderFilter.setDistance(25);
		centivoSelectProviderFilter.setFilterTags(Arrays.asList("testData"));
		centivoSelectProviderFilter.setFirstName("john");
		centivoSelectProviderFilter.setGroup(Arrays.asList("string"));
		centivoSelectProviderFilter.setLanguageSpoken("English");
		centivoSelectProviderFilter.setLastName("Rambo");
		centivoSelectProviderFilter.setMaxYearsPracticing(0);
		centivoSelectProviderFilter.setMinYearsPracticing(0);
		request.setFilter(centivoSelectProviderFilter);
		
		request.setSpecialtyList(Arrays.asList("Cardilogists"));
		SearchLocation requestLocation = new SearchLocation();
		requestLocation.setAddress("27217");
		GeoCoordinates geoLocation = new GeoCoordinates();
		geoLocation.setLattitude(0.0);
		geoLocation.setLongitude(0.0);
		requestLocation.setGeoLocation(geoLocation);
		request.setRequestLocation(requestLocation);
		request.setSessionToken("201905080229_208015d8-f1ed-4e97-8752-6ac423f70737");
		
		return request;
	}
*/

}
