package com.casion.stncasino;

import com.casion.framework.StnCasinoApplication;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;


@SpringBootTest(classes={StnCasinoApplication.class})
class StnCasinoApplicationTests {

	@Test
	void contextLoads() {
	}

}
