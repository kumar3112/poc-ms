package com.casion.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;


@Data
@Entity
public class User {
    @Id
    private Long userId;
    private String userName;
}
