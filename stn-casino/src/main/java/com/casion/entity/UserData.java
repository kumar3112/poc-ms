package com.casion.entity;

import lombok.Data;

import java.util.List;

@Data
public class UserData {
    private List<User> users;
}
