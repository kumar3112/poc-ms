package com.casion.service;


import com.casion.entity.User;
import com.casion.entity.UserData;
import com.casion.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.time.Instant;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    public UserData getAllUsers(){
        UserData userData = new UserData();
        List<User> user = (List<User>) userRepository.findAll();
        userData.setUsers(user);
        return userData;
    }

    public void addUser(User user){
        userRepository.save(user);
    }
}
