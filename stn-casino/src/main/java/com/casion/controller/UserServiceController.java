package com.casion.controller;

import com.casion.entity.User;
import com.casion.entity.UserData;
import com.casion.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/casino")
public class UserServiceController {

    @Autowired
    private UserService userService;

    @RequestMapping("/getUsers")
    public UserData getUser() {
       return userService.getAllUsers();
    }

    @PostMapping(path="/addUser", consumes={"application/json"})
    public void saveUser(@RequestBody User user) {
        userService.addUser( user);
    }
}
