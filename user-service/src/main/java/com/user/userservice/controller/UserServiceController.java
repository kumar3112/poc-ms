package com.user.userservice.controller;

import com.user.userservice.dao.UserDao;
import com.user.userservice.entity.User;
import com.user.userservice.entity.UserData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class UserServiceController {
   @Autowired
   UserDao dao;

    @RequestMapping("/getUser")
    public UserData getUser() {
       List<User> user = (List<User>) dao.findAll();
       UserData userData = new UserData();
       userData.setUsers(user);
       return userData;
    }

 @PostMapping(path="/user", consumes={"application/json"})
    public void postUser(@RequestBody User user) {
        dao.save(user);
    }
}
