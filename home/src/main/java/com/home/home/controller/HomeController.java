package com.home.home.controller;

import com.home.home.dto.CategoryData;
import com.home.home.dto.UserData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;


@RestController
public class HomeController {

    @Autowired
    RestTemplate restTemplate;

    @CrossOrigin(origins = "http://localhost:3000")
    @RequestMapping("/getUser")
    public UserData getUser() {
      UserData dto = restTemplate.getForObject("http://eureka-user-service/getUser", UserData.class);
       return dto;
    };

   @RequestMapping("/getCategory")
    public CategoryData getCategory() {
       RestTemplate restTemplate = new RestTemplate();
       CategoryData data = restTemplate.getForObject("http://localhost:8003/category", CategoryData.class);
       return data;
   }

}
