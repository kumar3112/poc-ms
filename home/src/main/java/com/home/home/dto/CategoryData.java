package com.home.home.dto;

import java.util.List;

public class CategoryData {
    List<CategoryDto> category;

    public List<CategoryDto> getCategory() {
        return category;
    }

    public void setCategory(List<CategoryDto> category) {
        this.category = category;
    }
}
