package com.home.home.dto;



public class UserDto {


    private int UserId;
    private String UserName;

    public UserDto(int userId, String userName) {
        UserId = userId;
        UserName = userName;
    }

    public UserDto() {

    }

    public int getUserId() {
        return UserId;
    }

    public void setUserId(int userId) {
        UserId = userId;
    }

    public String getUserName() {
        return UserName;
    }

    public void setUserName(String userName) {
        UserName = userName;
    }
}
